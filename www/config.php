<?php

date_default_timezone_set('Europe/Riga');

$url_count_succeeded = 'http://192.168.88.65:8080/alarm/count_succeeded/';
$url_process = 'https://bpayprocessing.iamoffice.lv/api/v1/invoice/process';
$url_reverse = 'https://bpayprocessing.iamoffice.lv/api/v1/invoice/reverse/';

$timeout = 10;
$connect_timeout = 2;

$minutes = '15';

$order_id = 'alarm_' . date('Y_m_d_H_i_s', time());
$amount = '0.10';
$currency = 'EUR';
$payment_method = 'FD_SMS';
$shop_name = 'alarm';

$cardholder = 'Francois Petrac';
$pan = '4314220000000049';
$cvc = '123';
$expiry = '0118';

$ShopPassword = 'alarm123';
