<?php
require_once('./config.php');
require_once('./log_txt.php');

// https://localdev.bilderlingspay.com/alarm.php
// https://secure-test.bilderlingspay.com/alarm/

// https://bpayprocessing.iamoffice.lv/alarm/count_succeeded/15

//  *****************************  Get count of succ transactions for 15 minutes  *****************************

LogInstance('alarm')->write('######### Start ############' . "\n" . '$minutes = ' . $minutes);

$testservice = false;
if (!$testservice) {
    $url = $url_count_succeeded . $minutes;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);

    $result = curl_exec($ch);
    if (false === $result) {
        LogInstance('alarm')->write('$url = ' . $url. "\ncurl_errno = " .curl_errno($ch)."\ncurl_error = ".curl_error($ch));
    }

    curl_close($ch);
    LogInstance('alarm')->write('$result = ' . $result);

    if ($result > 0) {
        LogInstance('alarm')->write('Success!!! (Exit)');
        die();
    }
}
//  ***************************** Payment and Reversal  *****************************

LogInstance('alarm')->write('Start [Payment]');

$bytes = openssl_random_pseudo_bytes(20);
$xid = base64_encode($bytes);
$X_Nonce = str_replace(array('+', '/', '='), '1', $xid);

$input = $order_id . $amount . $currency . $payment_method . $shop_name . $X_Nonce . $ShopPassword;

$sha512 = hash('sha512', $input, false);

$X_Request_Signature = $sha512;

$requestArray = array(
    'order_id' => $order_id,
    'amount' => $amount,
    'currency' => $currency,
    'payment_method' => $payment_method,
    'shop_name' => $shop_name,
    'cardholder' => $cardholder,
    'pan' => $pan,
    'cvc' => $cvc,
    'expiry' => $expiry
);

$requestJson = json_encode($requestArray);

$headers = array(
    'Content-Type: application/json',
    'Accept: application/json',
    'Connection: keep-alive',
    'X-Shop-Name: ' . $shop_name,
    'X-Nonce: ' . $X_Nonce,
    'X-Request-Signature: ' . $X_Request_Signature);

$ch = curl_init($url_process);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);

$result = curl_exec($ch);
if (false === $result) {
    LogInstance('alarm')->write('$url_process = ' . $url_process. "\ncurl_errno = " .curl_errno($ch)."\ncurl_error = ".curl_error($ch));
}

curl_close($ch);

LogInstance('alarm')->write('$result = ' . $result);

$response = json_decode($result, true);

LogInstance('alarm')->write('$result = ' . print_r($response, 1));
LogInstance('alarm')->write('Start [Reversal]');

// ********************   REVERSE   ******************************************
$alarm = true;
if (isset($response['invoice']['invoice_ref'])) {
    if (isset($response['payment_transaction']['status'])) {
        if ($response['payment_transaction']['status'] == 'SUCCEEDED') {
            $alarm = false;
            $invoice_ref = $response['invoice']['invoice_ref'];
            $url = $url_reverse . $invoice_ref;

            $bytes = openssl_random_pseudo_bytes(20);
            $xid = base64_encode($bytes);
            $X_Nonce = str_replace(array('+', '/', '='), '1', $xid);

            $input = $invoice_ref . $amount . $currency . $X_Nonce . $ShopPassword;

            $sha512 = hash('sha512', $input, false);

            $X_Request_Signature = $sha512;

            $requestArray = array(
                'amount' => $amount,
                'currency' => $currency
            );

            $requestJson = json_encode($requestArray);

            $headers = array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Connection: keep-alive',
                'X-Shop-Name: ' . $shop_name,
                'X-Nonce: ' . $X_Nonce,
                'X-Request-Signature: ' . $X_Request_Signature);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);

            $result = curl_exec($ch);
            if (false === $result) {
                LogInstance('alarm')->write('$url_reverse = ' . $url. "\ncurl_errno = " .curl_errno($ch)."\ncurl_error = ".curl_error($ch));
            }

            curl_close($ch);

            $response = json_decode($result, true);
            LogInstance('alarm')->write('$response = ' . print_r($response, 1));

        }
    }
}

if (!$alarm) {
    LogInstance('alarm')->write('Success after Payment and Reversal!!! (Exit)');
    die();
}

global $SETTINGS;
$SETTINGS = mailsettings();

require_once("./mailer/class.phpmailer.php");
require_once("./mailer/class.pop3.php");
require_once("./mailer/class.smtp.php");
require_once("./mailer/functions.php");

$to = 'av@bilderlingspay.com';
$subj = 'AlarmTagir';
$msg = 'No transactions on new system';

LogInstance('alarm')->write('Sending mail...');

SmartMail($to, $subj, $msg);
LogInstance('alarm')->write('Sent error mail');

die();

function LogInstance($id)
{
    static $logs = array();
    $path = './logs';

    //Создаем пути, тобы было легче ориентироваться в логах
    if (!is_dir($path = $path . '/' . date('Y'))) {
        @mkdir($path, 0755);
    }

    if (!is_dir($path = $path . '/' . date('m'))) {
        @mkdir($path, 0755);
    }

    if (!is_dir($path = $path . '/' . date('d'))) {
        @mkdir($path, 0755);
    }

    //Создаем пустой файл, если нет
    @fclose(fopen($file = $path . '/' . $id . '.log', 'a'));
    chmod($file, 0644);

    return new LOGFile($file);

}

function bydef(&$a, $i, $def = null)
{
    return isset($a[$i]) ? $a[$i] : $def;
}

function mailsettings()
{
    return array(

        'from_email' => 'info@bilderlingspay.com',
        'support_email' => 'info@bilderlingspay.com',
        'billing_email' => 'info@bilderlingspay.com',
        'admin_email' => 'info@bilderlingspay.com',
        'webmaster_email' => 'info@bilderlingspay.com',
        'error_email' => 'ag@bilderlingspay.com',
        'mail_send_settings' => array(
            'methods' => array(
                'trust' => 'GMAIL',
                'std' => 'SMTP',
                'alt' => 'MAIL'
            ),
            'templates' => array(
                'order_start' => 'trust',
                'change_password' => 'trust',
                'change_email' => 'trust',
                'user_add_register' => 'trust',
                'user_entryurl' => 'trust',

                'order_start_fail' => 'std',
                'order_complete' => 'std',
                'order_payback' => 'std',
                'user_add_register_admin_notify' => 'std',
            ),
            'transport' => array(

                'MAIL' => array(
                    'From' => ''
                ),
                'SMTP' => array(
                    'Host' => 'bilderlingspay.com',
                    'Port' => '25',
                    'Login' => 'ts@bilderlingspay.com',
                    'Passw' => 'nEsq8p1VzG',
                    'From' => '',
                ),
                'GMAIL' => array(
                    'Host' => 'smtp.gmail.com',
                    'Port' => '465',
                    'Secure' => "ssl",
                    'From' => '',
                    'Login' => '',
                    'Passw' => '',
                )

            )
        ),
        'smsender' => array(
            'smsc' => array(
                'login' => '<smsc.ru_login>',
                'password' => '<md5_password>',
                'debug' => 0,
                'sender' => null
            )
        ),
        'bing' => array(
            'api_id' => '7F6E7B1E1AE3E5C13919F5C5879C1DCE8DC0D494'
        )
    );
}
