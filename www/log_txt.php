<?php
// logging /
class LOGFile
{
 const DATE_FORMAT = '[d.m.Y H:i:s]';

 private $file;
 private $filename;

 public function __construct($filename)
 {
  $this->file = fopen($this->filename=$filename, 'a+');
 }

 public function __destruct()
 {
  if (isset($this->file)) fclose($this->file);
 }

 public function write($string, $w_time = true)
 {
  $lstr = $w_time === true ? date(LOGFile::DATE_FORMAT) . ' ' : '';
  $lstr = $lstr . $string . "\n";
  fwrite($this->file, $lstr);
  $GLOBALS['DEBUG_LOG_REPORTS'][] = $lstr;
        
 }
 public function get_contents()
 {
  return join("\n", $GLOBALS['DEBUG_LOG_REPORTS']);
 }
 public function Filename() { return $this->filename; }
    
    function curl_send_log($url, $POST)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
        
        curl_exec($ch);
        
        curl_close($ch);        
    }
}
?>
