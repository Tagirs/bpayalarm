<?php

/**
 *
 * @staticvar classname $mengines
 * @param string $id
 * @return manualEngine
 */
//------------------------------------------------------------------------------
//
function SmartMail($to, $subj, $msg, $hdr = '', $opts = null)
{
    if (SmartMailEx('SMTP', $to, $subj, $msg, $hdr)) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
//
function SmartMailEx($method, $to, $subj, $msg, $hdr = '')
{
    require_once('class.phpmailer.php');
//------------------------------------------------------------------------------
    global $SETTINGS;
    $CFG = $SETTINGS['mail_send_settings']['transport'];
//------------------------------------------------------------------------------
    $cfg = $CFG[$method];

    $headers = parse_mail_headers($hdr);

    $mail = new PHPMailer(); // the true param means it will throw exceptions on errors, which we need to catch

    if (!empty($cfg['Host'])) {
        $mail->IsSMTP();                            // telling the class to use SMTP
        $mail->SMTPDebug = 0;                        // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                    // enable SMTP authentication
        $mail->Host = $cfg['Host'];            // sets GMAIL as the SMTP server
        $mail->Username = $cfg['Login'];            // GMAIL username
        $mail->Password = $cfg['Passw'];        // GMAIL password
        if (!empty($cfg['Secure'])) $mail->SMTPSecure = $cfg['Secure'];
        if (!empty($cfg['Port'])) $mail->Port = $cfg['Port'];
    } else {
        $mail->IsMail();
    }

    if (!empty($headers['from'])) {
        $From = explode(' ', $headers['from'], 2);
        unset($headers['from']);
    }
    if (!empty($headers['reply-to'])) {
        $Reply = explode(' ', $headers['reply-to'], 2);
        $mail->AddReplyTo($Reply[0], trim(bydef($Reply, 1)));
        unset($headers['reply-to']);
    } elseif (!empty($From)) {
        $mail->AddReplyTo($From[0], trim(bydef($From, 1)));
    }

    if (!empty($cfg['From'])) {
        $From = explode(' ', $cfg['From'], 2);
        $mail->SetFrom($From[0], trim(bydef($From, 1)));
    }

    if (!empty($headers['content-type'])) {
        $ct = explode(';', strtolower($headers['content-type']), 2);
        $mail->IsHTML($ct[0] == 'text/html');
        if (!empty($ct[1]) && sizeof($ct = explode('charset=', trim($ct[1]))) > 1) {
            $mail->CharSet = trim($ct[1], ' ;"\'');
        }
        unset($headers['content-type']);
    } else {
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
    }

    $To = explode(' ', $to, 2);
    $mail->AddAddress($To[0], trim(bydef($To, 1)));

    $mail->Subject = $subj;
    $mail->Body = $msg;

    foreach ($headers as $n => $v) {
        $mail->AddCustomHeader(ucfirst($n) . ': ' . $v);
    }

    if (!$mail->Send()) {
        trigger_error("Mailer Error: " . $mail->ErrorInfo, E_USER_WARNING);
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
//
function parse_mail_headers($headers)
{
    $aHeaders = array();
    foreach (explode("\r\n", trim($headers)) as $h) if ($h) {
        list($n, $v) = explode(': ', $h, 2);
        if ($n) {
            $aHeaders[strtolower($n)] = $v;
        }
    }
    return $aHeaders;
}

/* SENDING E-MAILS */
function SendEMail($email, $subject, $msg, $from, $format = "html")
{
    $headers = "From: $from\r\n";
    $headers .= "Reply-To: $from\r\n";
    $headers .= "Content-Type: text/" . ($format == "html" ? "html" : "plain") . "; charset=" . CHARSET . "\r\n";

    return SmartMail($email, $subject, $msg, $headers);
}

function CUrl($url, $method, $params, $return = false)
{
    set_time_limit(0);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);

    if (strtoupper($method) == 'GET') {
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
    } else {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    }

    $contents = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $err_no = curl_errno($ch);
    $error = curl_error($ch);
    curl_close($ch);

    $arr = array('code' => $code, 'content' => $contents, 'error_no' => $err_no, 'error' => $error);
    return $arr;
}

